copy /a ..\sql\updates\world\4.3.4\*.sql /b world_updates.sql
copy /a ..\sql\updates\auth\4.3.4\*.sql /b a_updates.sql
copy /a ..\sql\updates\characters\4.3.4\*.sql /b ch_updates.sql
copy /a ..\sql\updates\hotfixes\4.3.4\*.sql /b h_updates.sql